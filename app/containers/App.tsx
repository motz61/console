import React, { ReactNode } from 'react';
import { Menu, Container, Dropdown } from 'semantic-ui-react';
import { useHistory } from 'react-router';
import routes from '../constants/routes.json';

type Props = {
  children: ReactNode;
};

export default function App(props: Props) {
  const history = useHistory();
  const { children } = props;
  return (
    <>
      <Menu fixed="top" inverted>
        <Container>
          <Menu.Item header>QC Console</Menu.Item>
          <Menu.Item onClick={() => history.push(routes.HOME)}>Home</Menu.Item>
          <Menu.Item onClick={() => history.push(routes.COUNTER)}>
            Counter
          </Menu.Item>

          <Menu.Menu position="right">
            <Dropdown item text="Servers">
              <Dropdown.Menu>
                <Dropdown.Item icon="check" text="Localhost" />
                <Dropdown.Item text="QuantumCore PTR" />
                <Dropdown.Divider />
                <Dropdown.Item text="Add server" />
              </Dropdown.Menu>
            </Dropdown>
          </Menu.Menu>
        </Container>
      </Menu>
      <Container>{children}</Container>
    </>
  );
}
